<!DOCTYPE html>
<html lang="es">
    
    <head>
	<meta charset="utf-8" >
	<meta name="description" content="Página Principal Internado Seccion B Pentathlon">
	<meta name="keywords" content="HTML5, CSS3, JavaScript">
	<title> Internado Secccion B Doctor Guztavo Baz Prada </title>
	<link rel="shortcut icon" type="image/x-icon" href="../multimedia/logopasantes.png">
	<?php
	include ("../functions/hubicacontenido.php");
	generarestilos();
	?>
	
    </head>

    <body>
	<?php
	include ("../frames/textooculto.php" );
	?>

	<div id="agrupar">
	    
	    <?php
	    
	    include ( "../frames/encabezado.php" );
	    include ( "../frames/menu.php" );
	    hubicacontenido();
	    
	    include ( "../frames/pie.php" );
	    ?>
	</div>
    </body>
</html>

