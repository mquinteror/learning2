<header id ="cabecera">
    <div id="logo">
	<img src="../multimedia/logodorado.png"/>
    </div>
    <div id="textocabecera">
	INTERNADO SECCIÓN "B"
	<br>
	DOCTOR GUSTAVO BAZ PRADA
	<br>
	PENTATHLÓN DEPORTIVO MILITARIZADO UNIVERSITARIO
    </div>
    <div id="redessociales">
	<a href="https://www.facebook.com/mesa.pasantes">
	    <img src="../multimedia/logo-face-black.png" width="35px" height="35px" alt ="FB"/>
	</a>
	<a href="https://www.youtube.com/channel/UClNIQ4rXXbGq7UQkcvA1Eag">
	    <img src="../multimedia/logo-youtube-black.png" width="35px" height="35px" alt="YOUTUBE"/>
	</a>
	<a href="https://twitter.com/isb_pdmu">
	    <img src="../multimedia/logo-twit-black.png" width="35px" height="35px" alt="TWIT" />
	</a>
</header>
