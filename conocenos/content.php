<section id="seccion">
    <div id="enfasispublico">
	Conoce al Internado Sección "B" del Pentathlon Deportivo Militarizado Universitario
    </div>
    <div id ="seccion1">
	<br>
	Somos Un Internado del Pentathlón deportivo Militarizado Universitario.
	<br>
	<br>
	 Por lo tanto nos regímos por los reglamentos del mísmo, así como participar de todas sus actividades e inundarnos de su filosofia, misión, valores, e ideología.
	<br>

	<br>
    </div>
    <hr>

    <div id ="seccion2">
	<div id="seccion2azul">
	    <div id="titulocafe">
		Un Cuerpo del P.D.M.U
	    </div>

	    <a href="../multimedia/penta/bandota.jpg" alt="HBG">
		<img class="imgmedianaderecha" src="../multimedia/penta/bandota.jpg" />
	    </a>
	    
	    <br>
	    Somos un Cuerpo más del PDMU, con la particularidad que sómos todos y obligatoriamente: Estudiantes.
	    <br>
	    Dentro del Pentathlón participamos en sus distintas pruebas y diciplinas.
	    <br>
	    <div id ="negris">
		Contamos con los siguíentes grupos
	    </div>
	</div>

	<div id="seccion2verde">
	    <a href="../multimedia/penta/banda01.JPG" alt="HBG Hidalgo">
		<img class="imgmedianaderecha" src="../multimedia/penta/banda01.JPG" />
	    </a>
	    <div id="negris">
		Banda de Guerra
	    </div>
	    Elemento Fundamental de loc Cuerpos de Tropa e infanteía
	    <br>
	    La banda de Guerra del I.S."B" Tiene una Tradición de Varias décadas y ha conseguido para nuestra gran cantidad de premios y reconocimientos
	    <br>
	    Convención Hidalgo
	    <br>
	</div>

	<div id="seccion2azul">
	    <a href="../multimedia/penta/escolta01.jpg" alt="Escolta">
		<img class="imgmedianaderecha" src="../multimedia/penta/escolta01.jpg" />
	    </a>
	    <div id="negris">
		Escolta Nacional
	    </div>
	    La escolta del Internado tiene el honor de portar la bandera guion en la columna del P.D.M.U
	    <br>
	    La gallardia que distingue las escoltas Méxicanas es uncomparable.
	    <br>
	</div>

	<div id="seccion2cafe">
	    <a href="../multimedia/penta/escolta01.jpg" alt="PruebaDeportiva">
		<img class="imgmedianaderecha" src="../multimedia/penta/escolta01.jpg" />
	    </a>
	    <div id="negris">
		Prueva Deportiva
	    </div>
	    La escolta del Internado tiene el honor de portar la bandera guion en la columna del P.D.M.U
	    <br>
	    La gallardia que distingue las escoltas Méxicanas es uncomparable.
	    <br>
	</div>

	<div id="seccion2verde">
	    <a href="../multimedia/penta/escolta01.jpg" alt="PruebaDeportiva">
		<img class="imgmedianaderecha" src="../multimedia/penta/escolta01.jpg" />
	    </a>
	    <div id="negris">
		Prueva Ideológica
	    </div>
	    La escolta del Internado tiene el honor de portar la bandera guion en la columna del P.D.M.U
	    <br>
	    La gallardia que distingue las escoltas Méxicanas es uncomparable.
	    <br>
	</div>


	
    </div>


    


    

    <div id="seccion2">
	<div id="tituloazul">
	    Residencia Para Estudiantes
	</div>

	<a href="../multimedia/penta/jardin01.JPG" alt="Jardin">
	    <img class="imgmedianaderecha" src="../multimedia/penta/jardin01.JPG" />
	</a>
	
	<br>
	Contribuir a que jóvenes originarios del interior de la república culminen satisfacotiamente sus estudios profesionales.
	<br>
	Reforzando la comunidad pentathónica. Y sobre todo: Conntribuyendo al Desarrollo del país.
	<br>
	<br>
	Contamos con una excelente infraestructura al servicio de los estudiantes


	<div id="seccion2cafe">
	    <a href="../multimedia/penta/cuadras01.JPG" alt="Dormitorios">
		<img class="imgmedianaderecha" src="../multimedia/penta/cuadras01.JPG" />
	    </a>
	    <div id="negris">
		Servicio de Alojamiento ( Dormitorios )
	    </div>
	    Los estudiantes del interior gástan la mayor parte de sus ingresos en pagar rentas abusivas.
	    <br>
	    Se Asigna a los Estudiantes:
	    <br>
	    Uno Dormitorio Permanente.
	    <br>
	    Un locker Para Guardar sus Pertenencias.
	    <br>
	    Mobiliario de Uso comun dentro del dormitorio.
	</div>

	
	<div id="seccion2verde">
	    <a href="../multimedia/penta/biblioteca01.JPG" alt="Biblioteca">
		<img class="imgmedianaderecha" src="../multimedia/penta/biblioteca01.JPG" />
	    </a>
	    <div id="negris">
		Biblioteca Disponible las 24 horas.
	    </div>
	    Nuestra biblioteca es cómoda y espaciosa.
	    <br>
	    Pero lo mas importante, es que puede hacerse uso de ella sin ninguna restricción de horario.
	    <br>
	</div>

	<div id="seccion2azul">
	    <a href="../multimedia/penta/comedor01.jpg" alt="Comedor">
		<img class="imgmedianaderecha" src="../multimedia/penta/comedor01.jpg" />
	    </a>
	    <div id="negris">
		Servicio de Comedor
	    </div>
	    El servicio de Cómedor proporciona a los Internos tres comidas al dia: Desyuno, Comida y Cena.
	    <br>
	    Contar con una buena alimentación es fundamental. Es por eso que es el servicio mas mimportante del ISB solo despues del Alojamiento.
	</div>





	<div id="seccion2">
	    <div id ="tituloverde">
		Actividades Deportivas Representativas
	    </div>
	    <div id="seccion2azul">
		<a href="../multimedia/penta/comedor01.jpg" alt="Comedor">
		    <img class="imgmedianaderecha" src="../multimedia/penta/comedor01.jpg" />
		</a>
		<div id="negris">
		    Botella
		</div>
		El servicio de Cómedor proporciona a los Internos tres comidas al dia: Desyuno, Comida y Cena.
		<br>
		Contar con una buena alimentación es fundamental. Es por eso que es el servicio mas mimportante del ISB solo despues del Alojamiento.
	    </div>
	    
	</div>
	<div id="seccion2verde">
	    <a href="../multimedia/penta/comedor01.jpg" alt="Comedor">
		<img class="imgmedianaderecha" src="../multimedia/penta/comedor01.jpg" />
	    </a>
	    <div id="negris">
		Tumbling
	    </div>
	    El servicio de Cómedor proporciona a los Internos tres comidas al dia: Desyuno, Comida y Cena.
	    <br>
	    Contar con una buena alimentación es fundamental. Es por eso que es el servicio mas mimportante del ISB solo despues del Alojamiento.
	</div>

	
    </div>
</section>













<section id ="seccion">
    <div id="seccion1">
	<div id ="titulo1verde">
	    Simbiósis
	    <br>
	    La Disciplina Militarizada y la Exceléncia Académica
	</div>
	<img class="imggrandederecha" src="../multimedia/universidad/ciudad-universitaria.jpg"/>
	<br>
	Tomar la Desición de estudiar fuera de casa. Muchas veces a miles de kilometros de distancia
	es una desisión dificil que pocos jóvenes se atreven a tomar. Para aquellos valientes el ISB Tiene todos Los servicios Necesarios.
	<br>
	La formación dentro de una Institución Militarizada tiene muchas ventajas. sobre todo con el desarrollo del carácter y la disciplina.
	<br>
    </div>
</section>
<section id ="seccion">
    <div id ="seccion2azul">
	<div id ="titulocafe">
	    Perfil de Ingreso
	</div>
	El estudiante que desee ingresar al I.S."B"
	Dede de ser una persona responsable, con actritud de servicio
    </div>

    <div id ="seccion2azul">
	<div id ="tituloverde">
	    Perfil de Egreso
	</div>
	El Estudiante que Culmina sus estudios superiores habiendo sido miembro activo del Internado, Cuenta con una formación que exalta la discipina, los valores cívicos y el amor a la patria.
    </div>

</section>

<section id="seccion">
    <div id="seccion1azul">
	<div id="titulo1azul">
	    Tradiciónes y Eventos
	    <br>
	    Importantes
	</div>
	Las tradiciones forman parte de nuestra identidad

	<div id="seccion1cafe">
	    <div id="titulo1verde">
		Aniversario
	    </div>
	    La Fiesta de Aniversarios es el evento en el cual se graduan los pasantes.
	    <br>
	    <br>
	    Los pasantes son los internos que han conluido sus carreras.
	</div>

	
	<div id="seccion1cafe">
	    <div id="titulo1verde">
		Convención Nacional
	    </div>
	    La Fiesta de Aniversarios es el evento en el cual se graduan los pasantes.
	    <br>
	    <br>
	    Los pasantes son los internos que han conluido sus carreras.
	</div>

	<div id="seccion1cafe">
	    <div id="titulo1verde">
		Práctica de Campo al Volcan Iztaccíuatl
	    </div>
	    La Fiesta de Aniversarios es el evento en el cual se graduan los pasantes.
	    <br>
	    <br>
	    Los pasantes son los internos que han conluido sus carreras.
	</div>

	<div id="seccion1azul">
	    <div id="titulo1verde">
		Bienvenida a Personal de nuevo Ingreso
	    </div>
	    La Fiesta de Aniversarios es el evento en el cual se graduan los pasantes.
	    <br>
	    <br>
	    Los pasantes son los internos que han conluido sus carreras.
	</div>

	<div id="seccion1azul">
	    <div id="titulo1verde">
		Preposada de fin de año
	    </div>
	    La Fiesta de Aniversarios es el evento en el cual se graduan los pasantes.
	    <br>
	    <br>
	    Los pasantes son los internos que han conluido sus carreras.
	</div>
    </div>
</section>

